package com.javatechig.intentserviceexample;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonParser {

	public String[] downloadData(String requestUrl) throws IOException,
			DownloadException {
		InputStream inputStream = null;

		HttpURLConnection urlConnection = null;

		/* forming th java.net.URL object */
		URL url = new URL(requestUrl);

		urlConnection = (HttpURLConnection) url.openConnection();

		/* optional request header */
		urlConnection.setRequestProperty("Content-Type", "application/json");

		/* optional request header */
		urlConnection.setRequestProperty("Accept", "application/json");

		/* for Get request */
		urlConnection.setRequestMethod("GET");

		int statusCode = urlConnection.getResponseCode();

		/* 200 represents HTTP OK */
		if (statusCode == 200) {
			inputStream = new BufferedInputStream(
					urlConnection.getInputStream());

			String response = convertInputStreamToString(inputStream);

			String[] results = parseResult(response);

			return results;
		} else {
			throw new DownloadException("Failed to fetch data!!");
		}
	}

	private String convertInputStreamToString(InputStream inputStream)
			throws IOException {

		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";

		while ((line = bufferedReader.readLine()) != null) {
			result += line;
		}

		/* Close Stream */
		if (null != inputStream) {
			inputStream.close();
		}

		return result;
	}

	private String[] parseResult(String result) {

		String[] blogTitles = null;
		try {
			JSONArray response = new JSONArray(result);

			blogTitles = new String[response.length()];

			for (int i = 0; i < response.length(); i++) {
				JSONObject post = response.getJSONObject(i);
				String name = post.optString("name_of_the_person");
				String title = post.optString("message");

				blogTitles[i] = title + " " + name;
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return blogTitles;
	}

	public class DownloadException extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public DownloadException(String message) {
			super(message);
		}

		public DownloadException(String message, Throwable cause) {
			super(message, cause);
		}
	}

}
